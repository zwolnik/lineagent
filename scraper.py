#!/usr/bin/python3

from datetime import datetime, timedelta
import pandas as pd
import requests as req
import json

TOKEN='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6IjM3Mjc1MjgzLTQ0ODUtNGJlNi05MjFhLWQwN2ZlZTMwN2IwYSIsImlhdCI6MTU4NTQyNDI4MSwic3ViIjoiZGV2ZWxvcGVyL2IxODBkYWQ0LWU3ZDQtZTA1Ni0zYmUzLTYzNjU2ZTFkM2QzMCIsInNjb3BlcyI6WyJyb3lhbGUiXSwibGltaXRzIjpbeyJ0aWVyIjoiZGV2ZWxvcGVyL3NpbHZlciIsInR5cGUiOiJ0aHJvdHRsaW5nIn0seyJjaWRycyI6WyIxNTYuMTcuOS43Il0sInR5cGUiOiJjbGllbnQifV19.Ic_SdODpt7p3FRMOFH5HsN96p2VxJ_DtwnyZMVTtz6q-ceBeXCM7baevjNErhG-VLqwSzqVbpAw2jxh7_go2Xg'
CLANTAG='%23YVR8GLG'

def crapi_get_json(path):
    res = req.get('https://api.clashroyale.com/v1/clans/{}{}'.format(CLANTAG, path),
                  headers={'Authorization': "Bearer {}".format(TOKEN)})
    if res.status_code != 200:
        raise 'Got wrong response for ' + path
    return json.loads(res.content.decode('utf-8'))

class Data:
    def __init__(self):
        self.members = None
        self.clan_info = None
        self.war_info = None
        self.war_log = None

        self.load_clan_info()
        self.load_members()
        self.load_current_war()
        self.load_warlog()

    def load_clan_info(self):
        data = crapi_get_json('')
        names   = ['Tag',    'Score',     'WarTrophies',     'Members', 'WeeklyDonations',  'RequiredTrophies', 'Type']
        targets = ['tag',    'clanScore', 'clanWarTrophies', 'members', 'donationsPerWeek', 'requiredTrophies', 'type']
        types   = ['object', 'int64',     'int64',           'int64',   'int64',            'int64',            'object']

        self.clan_info = pd.DataFrame(data=[data])
        self.clan_info.rename(columns={k:v for k,v in zip(targets, names)}, inplace=True)
        self.clan_info = self.clan_info[names]
        self.clan_info = self.clan_info.astype(dtype={k:v for k,v in zip(names, types)})

    def load_members(self):
        data = crapi_get_json('/members')
        data = pd.DataFrame(data=data['items'])
        names   = ['Name',   'Role',   'Seen',          'Donated',   'Received',          'Level',    'Trophies']
        targets = ['name',   'role',   'lastSeen',      'donations', 'donationsReceived', 'expLevel', 'trophies']
        types   = ['object', 'object', 'datetime64[s]', 'int64',     'int64',             'int64',    'int64']
        data.rename(columns={k:v for k,v in zip(targets, names)}, inplace=True)
        data = data.astype(dtype={k:v for k,v in zip(names, types)})
        data['Delta'] = data.Donated - data.Received
        names += ['Delta']
        self.members = data[names]

    def load_current_war(self): 
        data = crapi_get_json('/currentwar')
        if data['state'] == 'notInWar':
            self.war_info = pd.DataFrame(data=[data]).rename(columns={'state': 'State'})
            return

        for p in data['participants']:
            self.members.loc[self.members.Name == p['name'], 'cCards'] = p['cardsEarned']
            self.members.loc[self.members.Name == p['name'], 'cPlayed'] = p['battlesPlayed']
            self.members.loc[self.members.Name == p['name'], 'cWins'] = p['wins']
            self.members.loc[self.members.Name == p['name'], 'cCollPlayed'] = p['collectionDayBattlesPlayed']
            self.members.loc[self.members.Name == p['name'], 'cBattles'] = p['numberOfBattles']

        self.members = self.members.astype(
                dtype={k: pd.Int64Dtype() for k in ['cCards', 'cPlayed', 'cWins', 'cCollPlayed', 'cBattles']})

        names =   ['State',  'Score',     'Crowns', 'Participants', 'Played',        'Won']
        targets = ['state',  'clanScore', 'crowns', 'participants', 'battlesPlayed', 'wins']
        types =   ['object', 'int64',     'int64',  'int64',        'int64',         'int64']
        war_data = pd.DataFrame(data=[data['clan']])
        war_data['state'] = data['state']
        try:
            war_data['collectionEndTime'] = data['collectionEndTime']
            names +=   ['CollEnd']
            targets += ['collectionEndTime']
            types +=   ['datetime64[s]']
        except:
            pass
        try:
            war_data['warEndTime'] = data['warEndTime']
            names +=   ['WarEnd']
            targets += ['warEndTime']
            types +=   ['datetime64[s]']
        except:
            pass
        war_data.rename(columns={k:v for k,v in zip(targets, names)}, inplace=True)
        war_data = war_data[names]
        self.war_info = war_data.astype(dtype={k:v for k,v in zip(names, types)})

    def load_warlog(self):
        data = crapi_get_json('/warlog?limit=10')
        names =   ['Started',       'Season', 'Place', 'Trophies',     'Wins',  'Crowns', 'Battles',       'Participants']
        targets = ['createdDate',   'season', 'place', 'trophyChange', 'wins',  'crowns', 'battlesPlayed', 'participants']
        types =   ['datetime64[s]', 'int64',  'int64', 'int64',        'int64', 'int64',  'int64',         'int64']

        for name in ['Wars', 'Wins', 'CollMisses', 'WarDayMisses', 'Cards', 'Played', 'CollPlayed']:
            self.members[name] = [0 for _ in range(len(self.members.index))]

        self.war_log = pd.DataFrame(columns=names)
        for war in data['items']:
            participants = war['participants']
            for p in participants:
                self.members.loc[self.members.Name == p['name'], 'Wars'] += 1
                self.members.loc[self.members.Name == p['name'], 'Wins'] += p['wins']
                self.members.loc[self.members.Name == p['name'], 'CollMisses'] += 3-p['collectionDayBattlesPlayed']
                self.members.loc[self.members.Name == p['name'], 'WarDayMisses'] += p['numberOfBattles'] - p['battlesPlayed']
                self.members.loc[self.members.Name == p['name'], 'Cards'] += p['cardsEarned']
                self.members.loc[self.members.Name == p['name'], 'Played'] += p['battlesPlayed']
                self.members.loc[self.members.Name == p['name'], 'CollPlayed'] += p['collectionDayBattlesPlayed']

            for n, c in enumerate(war['standings']):
                if c['clan']['name'] == 'Lineage':
                    warstamp = pd.DataFrame(data=[c['clan']])
                    warstamp['season'] = war['seasonId']
                    warstamp['createdDate'] = war['createdDate']
                    warstamp['place'] = n+1
                    warstamp['trophyChange'] = c['trophyChange']
                    warstamp.rename(columns={k:v for k,v in zip(targets, names)}, inplace=True)
                    warstamp = warstamp[names]
                    warstamp = warstamp.astype(dtype={k:v for k,v in zip(names, types)})
                    self.war_log = pd.concat([self.war_log, warstamp], ignore_index=True)


class Analyser:
    def __init__(self):
        self.data = Data()
    
    def refresh(self):
        self.data = Data()

    def war_state(self):
        return self.data.war_info.loc[0]['State']

# General tab

    def top_donators(self, count=5):
        return self.data.members.nlargest(count, 'Delta')[
            ['Name', 'Donated', 'Received', 'Delta', 'Role']
        ]

    def worst_donators(self, count=5):
        return self.data.members.nsmallest(count, 'Delta')[
            ['Name', 'Donated', 'Received', 'Delta', 'Role']
        ]

    def under_100(self):
        return self.data.members[self.data.members.Donated.lt(100)][
            ['Name', 'Donated', 'Received', 'Delta', 'Role']
        ].sort_values('Donated')

    def top_missing(self, count=5):
        week_prior = pd.Timestamp.now() - pd.DateOffset(weeks=1)
        ret = self.data.members.nsmallest(count, 'Seen')
        return ret[self.data.members.Seen <= week_prior][
            ['Name', 'Seen', 'Wars', 'Played', 'Cards', 'Role']
        ]

# Current war tab

    def current_best_collectors(self, count=5):
        if self.war_state() == 'notInWar':
            return None
        else:
            return self.data.members.nlargest(count, 'cCards')[
                ['Name', 'cCards', 'cCollPlayed']
            ]

    def current_best_fighters(self, count=5):
        if self.war_state() != 'warDay':
            return None
        return self.data.members.nlargest(count, 'cWins')[
            ['Name', 'cWins', 'cPlayed', 'cBattles']
        ]

    def participating(self):
        if self.war_state() == 'notInWar':
            return None
        return self.data.members[
            self.data.members.cCollPlayed.notna()
        ][
            ['Name', 'cPlayed', 'cBattles', 'cWins', 'cCollPlayed', 'cCards']
        ].sort_values('cPlayed')

    def not_participating(self):
        if self.war_state() == 'notInWar':
            return None
        return self.data.members[
            self.data.members.cCollPlayed.isna()
        ][['Name', 'Wars', 'Played', 'CollPlayed']]

# Last 10 wars tab

    def top_collectors(self, count=5):
        return self.data.members.nlargest(count, 'Cards')[
            ['Name', 'Cards', 'Wars', 'CollPlayed']
        ]

    def top_fighters(self, count=5):
        return self.data.members.nlargest(count, 'Wins')[
            ['Name', 'Wars', 'Played', 'Wins', 'Cards']
        ]

    def war_day_sabotages(self):
        return self.data.members[self.data.members.WarDayMisses != (0)][
            ['Name', 'Wars', 'Played', 'Wins', 'WarDayMisses', 'CollMisses', 'Role']
        ].sort_values('WarDayMisses', ascending=False)

    def collection_day_sabotages(self):
        return self.data.members[self.data.members.CollMisses != (0)][
            ['Name', 'Wars', 'Played', 'Wins', 'CollMisses', 'WarDayMisses', 'Role']
        ].sort_values('CollMisses', ascending=False)

    def most_active(self, above=7):
        return self.data.members[self.data.members.Wars.gt(above)][
            ['Name', 'Wars', 'Played', 'Cards']
        ].sort_values('Wars', ascending=False)

    def least_active(self, below=4):
        return self.data.members[self.data.members.Wars.lt(below)][
            ['Name', 'Wars', 'Played', 'Cards']
        ].sort_values('Wars')

# Hall of flame tab

    def sacred_board(self, above=6):
        return self.data.members[
            (self.data.members.Wars.gt(above)) & (self.data.members.WarDayMisses == 0) & (self.data.members.CollMisses == 0)
        ][
            ['Name', 'Wars', 'Played', 'Wins', 'Cards', 'Role']
        ].sort_values('Wars', ascending=False)

    def best_players(self, count=5):
        return self.data.members.nlargest(count, 'Trophies')[
            ['Name', 'Trophies', 'Wars', 'Played', 'Wins', 'Level', 'Role']
        ]
