from flask import Flask, render_template, request
from scraper import Analyser

app = Flask(__name__)
app.jinja_env.filters['zip'] = zip

analyser = Analyser()

@app.route("/lineagent")
def main_page():
    donators_len = len(analyser.under_100()) // 2 - 1
    best_players_len = len(analyser.sacred_board(above=6))
    render_data = {
# Data
        'war_log':   analyser.data.war_log,
        'war_info':  analyser.data.war_info,
        'war_state': analyser.war_state(),
        'clan_info': analyser.data.clan_info,
# General tables
        'under_100':      analyser.under_100(),
        'top_missing':    analyser.top_missing(),
        'top_donators':   analyser.top_donators(count=sorted([7, donators_len, 15])[1]),
        'worst_donators': analyser.worst_donators(count=sorted([7, donators_len, 15])[1]),
# Current war tables 
        'participating':           analyser.participating(),
        'not_participating':       analyser.not_participating(),
        'current_best_fighters':   analyser.current_best_fighters(count=10),
        'current_best_collectors': analyser.current_best_collectors(count=10),
# Last 10 wars tables
        'most_active':              analyser.most_active(above=7),
        'least_active':             analyser.least_active(below=4),
        'top_fighters':             analyser.top_fighters(count=5),
        'top_collectors':           analyser.top_collectors(count=5),
        'war_day_sabotages':        analyser.war_day_sabotages(),
        'collection_day_sabotages': analyser.collection_day_sabotages(),
# Hall of flame tables
        'sacred_board': analyser.sacred_board(above=6),
        'best_players': analyser.best_players(count=best_players_len)
    }

    return render_template('base.html', **render_data)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5556)
